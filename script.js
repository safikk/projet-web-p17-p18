// Création du conteneur 
let cardSection = document.createElement('div');
cardSection.className = 'row m-4 p-3 justify-content-around';

// Injection du conteneur dans la page...
document.body.appendChild(cardSection);

// Créa tion et écoute du bouton de création d'articles
let button = document.getElementById('btn');
button.addEventListener("click", create)


function create() {

  // Recupération des inputs utilisateurs
  let inputTitre = document.getElementById('Titre').value;
  let inputAuthor= document.getElementById('Author').value;
  let inputContent= document.getElementById('Content').value;

  // Conditions de création d'un article
  if (inputTitre.length <= 20 && inputAuthor.length <= 20 ) {

    // Création des éléments 
    let cardBody = document.createElement('div');
    cardBody.className = 'card-body';

    let image = document.createElement('img');
    image.className = 'card-img-top';
    image.alt = 'Card image cap';
    image.src = 'img.jpg';
  
    let headerText = document.createElement('h3');
    headerText.textContent = inputTitre;
  
    let authorText = document.createElement('h4');
    authorText.textContent = inputAuthor;
  
    let paragraph = document.createElement('p');
    paragraph.className = 'card-body';
    paragraph.textContent = inputContent;

    let button = document.createElement('button');
    button.className = 'btn btn-primary';
    button.textContent = 'Display'
  
    let card = document.createElement('div');
    card.className = 'card col-12 col-xs-4 border my-4';
    card.style = 'width: 18rem;';
  
    // Injection des éléments dans le DOM
    cardBody.appendChild(headerText);
    cardBody.appendChild(authorText)
    cardBody.appendChild(paragraph);
    cardBody.appendChild(button);
    card.appendChild(cardBody);
    cardSection.appendChild(card);
  
  } else {

    // Alerte l'utilisateur en cas de non respect de la condition
    alert("Your Title and Author should not be longer than 20 characters !");

  }

}

